package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}

func readUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}

func main() {
	port := getEnv("PORT", "8080")
	greeting := getEnv("GREETING", "Hey")
	hostname, _ := os.Hostname()

	fmt.Fprintf(os.Stdout, "Listening on :%s\n", port)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ip := readUserIP(r)
		fmt.Fprintf(w, "%s %s! I'm %s.\n", greeting, ip, hostname)
		if r.Header.Get("X-Forwarded-For") != "" {
			fmt.Fprintf(w, "Your man in the middle is %s\n", r.RemoteAddr)
		}
	})

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
